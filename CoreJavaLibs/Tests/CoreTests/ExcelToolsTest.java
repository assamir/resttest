package CoreTests;
import java.util.Map;

import FileTools.ExcelTools;
import lombok.Data;

@Data
public class ExcelToolsTest {
    @org.junit.jupiter.api.Test
    public static void main(String[] args) {
        ExcelTools e = new ExcelTools("C:\\Users\\kain9\\OneDrive\\Desktop\\Lista zakupów.xlsx");
        Map<String,String> informations = e.GetWorkbookInformations();

        System.out.println("informations = " + informations);
        String state = e.getState();
        System.out.println("state = " + state);

        String version = e.getVersion();
        System.out.println("version = " + version);

        e.setSheet("Posiłki");
        Object cell = e.readCellContent(3,4);

        System.out.println("cell.toString() = " + cell.toString());
    }
}

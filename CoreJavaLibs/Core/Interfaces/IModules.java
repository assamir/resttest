package Interfaces;

import Enums.EModuleState;

public interface IModules {
    String version = "Not set";
    EModuleState state = EModuleState.Development;

    String getVersion();
    String getState();
}
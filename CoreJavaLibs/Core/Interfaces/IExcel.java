package Interfaces;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

public interface IExcel extends IModules {
    Map<String,String> GetWorkbookInformations();
    Integer getNumberOfSheets();
    List<String> getSheetNames();
    Integer setSheet(String name);

    Object readCellContent(Integer col, Integer row);
    BigDecimal readCellContentBigDecimal(Integer col, Integer row, Integer scale);

    List<String> getRow(Integer row);
    List<BigDecimal> getRowBigDecimal(Integer row);

    Integer writeCell(Integer col, Integer row, String value);
    Integer writeCellBigDecimal(Integer col, Integer row, BigDecimal value);

}
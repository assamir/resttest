package Enums;

public enum EModuleState {
    Development,
    Release_Candidate,
    Experimental,
    Stable,
    Deprecated
}
package FileTools;
//https://google.github.io/styleguide/javaguide.html
//https://plugins.jetbrains.com/plugin/8527-google-java-format/versions
import Enums.EModuleState;
import Interfaces.IExcel;
import lombok.Getter;
import lombok.Setter;
//import org.apache.poi.hssf.usermodel.HSSFWorkbook;
//import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//@link https://dzone.com/articles/introduction-to-apache-poi-library
//@link https://poi.apache.org/apidocs/3.17/
@Getter
@Setter
public class ExcelTools implements IExcel {
    protected XSSFWorkbook thisWorkbook = null;
    Sheet thisSheet = null;
    protected Integer numberOfSheets = -1;
    protected List<String> sheetNames = new ArrayList<>();
    String version = "0.1";
    EModuleState state = EModuleState.Development;

    protected String currentSheet = "brak";

    @Override
    public String getVersion() {
        return version;
    }

    @Override
    public String getState() {
        return String.valueOf(state);
    }

    public ExcelTools(String FilePath){
        XSSFWorkbook workbook = null;

        try{
            FileInputStream excelStrem = new FileInputStream(new File(FilePath));
            workbook = new XSSFWorkbook(excelStrem);
            //workbook = WorkbookFactory.create(new File(FilePath));
            this.thisWorkbook = workbook;
        } catch (Exception e) {
            System.out.println("e = " + e);
            System.out.println("e = " + e.getMessage());
            System.out.println("e = " + e.getCause());
        }
    }

    public Map<String,String> GetWorkbookInformations(){
        Map<String,String> res = new HashMap<>();

        Integer numberOfSheets = this.setNumberOfSheets();
        String sheetNames = this.setSheetNames();
        res.put("numberOfSheets",numberOfSheets.toString());
        res.put("sheetNames",sheetNames);

        return  res;
    }

    @org.jetbrains.annotations.NotNull
    private Integer setNumberOfSheets(){
        this.numberOfSheets = this.thisWorkbook.getNumberOfSheets();
        return this.thisWorkbook.getNumberOfSheets();
    }

    private String setSheetNames(){
        String res = "";
        StringBuilder stringBuilder = new StringBuilder();

        Integer sheetNumber = this.getNumberOfSheets();
        for (int i = 0; i < sheetNumber; i++) {
            String name = "";
            name = this.thisWorkbook.getSheetName(i);
            stringBuilder.append(name).append("|");
            this.sheetNames.add(name);
        }

        res = stringBuilder.toString();
        return res;
    }

    public Integer getNumberOfSheets(){
        return this.numberOfSheets;
    }
    public List<String> getSheetNames(){ return  this.sheetNames;}

    @Override
    public Integer setSheet(String name) {
        if( getSheetNames().contains(name) ){
            currentSheet = name;
            thisSheet = thisWorkbook.getSheet(name);
            return 1;
        } else {
            return 0;
        }
    }

    @Override
    public Object readCellContent(Integer col, Integer row) {

        Row wiersz = thisSheet.getRow(row);
        Cell komorka = wiersz.getCell(col);
        Object value = getCellValue(komorka);


        return value;
    }

    protected Object getCellValue(Cell komorka){
        Object res = null;

        CellType typkomorki = komorka.getCellType();//getCellTypeEnum();

        switch (typkomorki){
            case STRING:
                res = komorka.getRichStringCellValue().toString();
                break;

            case BLANK:
                res = null;
                break;

            case BOOLEAN:
                res = komorka.getBooleanCellValue();
                break;

            case NUMERIC:
                res = komorka.getNumericCellValue();
                break;

            default:
                System.out.println("Nie zdefiniowano typu komorki do odczytu: "+ typkomorki.toString());
                res = "Nie zdefiniowano typu komorki do odczytu: "+ typkomorki.toString();
                break;

        }

        return res;
    }

    @Override
    public BigDecimal readCellContentBigDecimal(Integer col, Integer row, Integer scale) {
        return null;
    }

    @Override
    public List<String> getRow(Integer row) {
        return null;
    }

    @Override
    public List<BigDecimal> getRowBigDecimal(Integer row) {
        return null;
    }

    @Override
    public Integer writeCell(Integer col, Integer row, String value) {
        return null;
    }

    @Override
    public Integer writeCellBigDecimal(Integer col, Integer row, BigDecimal value) {
        return null;
    }

}
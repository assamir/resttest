import Templatki.UserRest;
import Templatki.UsersList;
import io.restassured.RestAssured;
import io.restassured.mapper.ObjectMapper;
import io.restassured.response.Response;
import lombok.Data;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

import static io.restassured.RestAssured.*;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.core.IsIterableContaining.*;
@Data
public class RestAssuredTest {
    @Test
    public void testGet(){
        RestAssured.baseURI = "https://reqres.in/api";
        get("/users").then().assertThat().body("data.email",hasItem("george.bluth@reqres.in"));
        Response res = get("/users");
    }

    @Test
    public void testPost(){
        RestAssured.baseURI = "https://reqres.in/api";
        Map<String,String> user = new HashMap<>();
        user.put("name","Dyzio");
        user.put("job","Zjadacz chrupek");

        UserRest responseUser = new UserRest();

        UserRest responseString = with().contentType("application/json").body(user).when().request("POST","/api/users").then()
                .statusCode(201)
                .body("job",equalTo("Zjadacz chrupek"))
                .extract().as(UserRest.class);
//                .extract().asString();
        System.out.println("responseString = " + responseString);
    }

    @Test
    public void getUsersList(){
        RestAssured.baseURI = "https://reqres.in/api";
        UsersList ul = get("/users").getBody().as(UsersList.class);
//        System.out.println("responseBody = " + get("/users").getBody().as(UsersList.class).toString());
//        System.out.println("ul = " + ul);
        List<UserRest> ur = ul.getData();

        ur.forEach( item -> System.out.println("item = " + item) );
        Consumer<UserRest> printFullName = x -> System.out.println(x.getFullName());
        ur.forEach(printFullName);
    }

}

package Templatki;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

public @Data
//@JsonIgnoreProperties(ignoreUnknown = true)
class UserRest {
    Integer id;
    String job;
    String createdAt = "";
    String email;
    String first_name;
    String last_name;
    String avatar;
    String name;

    public String getFullName(){
        return first_name + " " + last_name;
    }
}

package Templatki;

import lombok.Data;
import java.util.List;

public @Data
class UsersList {
    Integer page;
    Integer per_page;
    Integer total;
    Integer total_pages;
    List<UserRest> data;

    public List<UserRest> getData() {
        return this.data;
    }
}
